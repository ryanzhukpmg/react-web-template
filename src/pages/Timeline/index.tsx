import React from 'react';
import Card from '../../components/Card';

const Timeline = () => (
  <Card
    url={`https://images.unsplash.com/photo-1584672277148-fa8d3b8e3780?ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80`}
  />
);

export default Timeline;
