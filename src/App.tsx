import React from 'react';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import Timeline from './pages/Timeline';

function Routes() {
  return (
    <div>
      <Route exact={true} path="/" component={Timeline} />
    </div>
  );
}

function App() {
  return (
    <Router>
      <Routes />
    </Router>
  );
}

export default App;
