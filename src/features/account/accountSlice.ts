import { createSlice } from '@reduxjs/toolkit';

interface AccountState {
  value: number;
}

const initialState: AccountState = {
  value: 0,
};

export const accountSlice = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    someAction: state => {
      state.value -= 1;
    },
  },
});

export const { someAction } = accountSlice.actions;

export default accountSlice.reducer;
