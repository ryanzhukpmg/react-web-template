import React from 'react';

const PageContainer = ({ children }: any) => (
  <div className="bg-gray-100 min-h-screen">{children}</div>
);

export default PageContainer;
