This project is using [Create React App](https://github.com/facebook/create-react-app), [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/).

## Installation

In the project directory, you can run:

1. `yarn`
2. `yarn start`
3. Open the web browser to http://localhost:3000/

## Run unit test

- `yarn test`

## Build

-  `yarn build`
